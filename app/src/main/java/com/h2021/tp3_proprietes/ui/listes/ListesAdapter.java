package com.h2021.tp3_proprietes.ui.listes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.h2021.tp3_proprietes.R;
import com.h2021.tp3_proprietes.model.Propriete;
import com.h2021.tp3_proprietes.ui.favoris.FavorisFragmentDirections;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListesAdapter extends RecyclerView.Adapter<ListesAdapter.ViewHolder> {

    private ArrayList<Propriete> lstProprietes;
    private Context mContext;


    public ListesAdapter(ArrayList<Propriete> lstProprietes) {
        this.lstProprietes = lstProprietes;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_listes, parent, false);
        return new ListesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        mContext = holder.itemView.getContext();
        Propriete propriete = lstProprietes.get(position);
        holder.tvListeType.setText(propriete.getType());
        holder.tvListePrix.setText(propriete.getPrice());
        holder.tvListeAdresse.setText(propriete.getAdresse());
        holder.tvListeCity.setText(propriete.getCity());
        Picasso.get().load(propriete.getPhotos().getPetit())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.ivListePhoto);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    NavDirections actionListes = ListesFragmentDirections.actionNavListesToDetailsFragment(propriete.getId());
                    Navigation.findNavController(v).navigate(actionListes);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }

                try {
                    NavDirections actionFavoris = FavorisFragmentDirections.actionNavFavorisToDetailsFragment(propriete.getId());
                    Navigation.findNavController(v).navigate(actionFavoris);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return lstProprietes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvListeType, tvListePrix, tvListeAdresse, tvListeCity;
        private ImageView ivListePhoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvListeType = itemView.findViewById(R.id.tv_liste_type);
            tvListePrix = itemView.findViewById(R.id.tv_liste_prix);
            tvListeAdresse = itemView.findViewById(R.id.tv_liste_adresse);
            tvListeCity = itemView.findViewById(R.id.tv_liste_city);
            ivListePhoto = itemView.findViewById(R.id.iv_listes_photo);
        }
    }
}
