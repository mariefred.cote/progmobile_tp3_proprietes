package com.h2021.tp3_proprietes.ui.details;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.h2021.tp3_proprietes.R;
import com.h2021.tp3_proprietes.data.AppExecutors;
import com.h2021.tp3_proprietes.data.ProprieteRoomDatabase;
import com.h2021.tp3_proprietes.model.Commentaire;

import static com.h2021.tp3_proprietes.R.string.erreur_comment;

public class DialogCommentaire extends DialogFragment {
    private Commentaire commentaire;
    private int idPropriete;
    private ProprieteRoomDatabase mDb;
    private EditText etCommentaire;

    public DialogCommentaire() {
    }

    public DialogCommentaire(Commentaire commentaire) {
        this.commentaire = commentaire;
    }

    public DialogCommentaire(int idPropriete) {
        this.idPropriete = idPropriete;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.add_commentaire, null);
        Context context = getContext();
        etCommentaire = view.findViewById(R.id.et_commentaire);
        mDb = ProprieteRoomDatabase.getDatabase(getContext());

        if (commentaire != null) {
            etCommentaire.setText(commentaire.getCommentaire());
            idPropriete = commentaire.getIdPropriete();
        }

        builder.setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String comment = etCommentaire.getText().toString().trim();

                        if (comment.isEmpty()){
                            Toast.makeText(context, erreur_comment, Toast.LENGTH_LONG).show();
                            return;
                        }
                        Commentaire commentaire = new Commentaire(idPropriete, comment);

                        AppExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                mDb.proprieteDao().insert(commentaire);
                            }
                        });
                    }
                })
                .setNegativeButton(R.string.annuler, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        builder.setTitle(R.string.ajouter_un_commentaire);

        return builder.create();
    }
}
