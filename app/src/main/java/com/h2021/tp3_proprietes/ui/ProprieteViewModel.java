package com.h2021.tp3_proprietes.ui;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.h2021.tp3_proprietes.data.ProprieteRoomDatabase;
import com.h2021.tp3_proprietes.model.Commentaire;
import com.h2021.tp3_proprietes.model.Propriete;

import java.util.List;

public class ProprieteViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;
    private LiveData<List<Propriete>> lstProprieteFav;
    private LiveData<List<Propriete>> lstPropriete;
    private ProprieteRoomDatabase mDb;

    public ProprieteViewModel(Application application) {
        super(application);
        mDb = ProprieteRoomDatabase.getDatabase(application);
        lstProprieteFav = mDb.proprieteDao().getProprietesFav();
        lstPropriete = mDb.proprieteDao().getProprietesLive();
    }

    public LiveData<Propriete> getPropriete(int idPropriete) {
        return mDb.proprieteDao().getPropriete(idPropriete);
    }

    public LiveData<Commentaire> getCommentaire(int idPropriete) {
        return mDb.proprieteDao().getCommentaire(idPropriete);
    }

    public LiveData<List<Propriete>> getLstProprieteFav() {
        return lstProprieteFav;
    }

    public LiveData<List<Propriete>> getLstPropriete() {
        return lstPropriete;
    }
}