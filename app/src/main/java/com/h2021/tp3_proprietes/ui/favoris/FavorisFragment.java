package com.h2021.tp3_proprietes.ui.favoris;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.h2021.tp3_proprietes.R;
import com.h2021.tp3_proprietes.model.Propriete;
import com.h2021.tp3_proprietes.ui.ProprieteViewModel;
import com.h2021.tp3_proprietes.ui.listes.ListesAdapter;

import java.util.ArrayList;
import java.util.List;

public class FavorisFragment extends Fragment {

    private ProprieteViewModel proprieteViewModel;
    private ListesAdapter listesAdapter;
    private RecyclerView rvFavoris;
    private ArrayList<Propriete> lstProprietesFav;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        proprieteViewModel = new ViewModelProvider(this).get(ProprieteViewModel.class);

        View root = inflater.inflate(R.layout.fragment_favoris, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvFavoris = view.findViewById(R.id.rv_favoris);
        lstProprietesFav = new ArrayList<>();
        listesAdapter = new ListesAdapter(lstProprietesFav);
        rvFavoris.setLayoutManager(new LinearLayoutManager(requireActivity()));
        rvFavoris.setAdapter(listesAdapter);

        proprieteViewModel.getLstProprieteFav().observe(getViewLifecycleOwner(), new Observer<List<Propriete>>() {
            @Override
            public void onChanged(List<Propriete> proprietes) {
                lstProprietesFav.addAll(proprietes);
                listesAdapter.notifyDataSetChanged();
            }
        });

    }
}