package com.h2021.tp3_proprietes.ui.details;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.h2021.tp3_proprietes.R;
import com.h2021.tp3_proprietes.data.AppExecutors;
import com.h2021.tp3_proprietes.data.ProprieteRoomDatabase;
import com.h2021.tp3_proprietes.model.Commentaire;
import com.h2021.tp3_proprietes.model.Propriete;
import com.h2021.tp3_proprietes.ui.ProprieteViewModel;
import com.squareup.picasso.Picasso;

public class DetailsFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener, GoogleMap.InfoWindowAdapter, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener {

    private ProprieteViewModel proprieteViewModel;
    private int id_propriete;
    private TextView tvId, tvType, tvPrix, tvAdresse, tvCity, tvCommentaire;
    private ImageView ivDetailPhoto;
    private ImageButton btFavOff, btFavOn;
    private FloatingActionButton btAddCom;
    private static final int LOCATION_PERMISSSION_CODE = 1;
    private GoogleMap mMap;
    private ProprieteRoomDatabase mDb;
    final private static String COMMENT = "Ajout commentaire";


    public DetailsFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        proprieteViewModel =
                new ViewModelProvider(requireActivity()).get(ProprieteViewModel.class);

        mDb = ProprieteRoomDatabase.getDatabase(getContext());

        View root = inflater.inflate(R.layout.fragment_details, container, false);

        ivDetailPhoto = root.findViewById(R.id.iv_details_photo);
        tvId = root.findViewById(R.id.tv_details_id2);
        tvType = root.findViewById(R.id.tv_details_type);
        tvPrix = root.findViewById(R.id.tv_details_prix);
        tvAdresse = root.findViewById(R.id.tv_details_adresse);
        tvCity = root.findViewById(R.id.tv_details_city);
        tvCommentaire = root.findViewById(R.id.tv_details_commentaire);
        btFavOff = root.findViewById(R.id.bt_details_favoris_off);
        btFavOn = root.findViewById(R.id.bt_details_favoris_on);
        btAddCom = root.findViewById(R.id.bt_details_addCom);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        id_propriete = DetailsFragmentArgs.fromBundle(getArguments()).getIdPropriete();

        proprieteViewModel.getPropriete(id_propriete).observe(getViewLifecycleOwner(), new Observer<Propriete>() {
            @Override
            public void onChanged(Propriete propriete) {
                tvId.setText(propriete.getId().toString());
                tvAdresse.setText(propriete.getAdresse());
                tvType.setText(propriete.getType());
                tvPrix.setText(propriete.getPrice());
                tvCity.setText(propriete.getCity());
                Picasso.get().load(propriete.getPhotos().getGrand())
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(ivDetailPhoto);
                if (propriete.getFavoris()) {
                    btFavOn.setVisibility(View.VISIBLE);
                    btFavOff.setVisibility(View.GONE);
                } else {
                    btFavOff.setVisibility(View.VISIBLE);
                    btFavOn.setVisibility(View.GONE);
                }

            }
        });

        proprieteViewModel.getCommentaire(id_propriete).observe(getViewLifecycleOwner(), new Observer<Commentaire>() {
            @Override
            public void onChanged(Commentaire commentaire) {
                if (commentaire != null){
                    tvCommentaire.setText(commentaire.getCommentaire());
                }
                else {
                    tvCommentaire.setText("");
                }
            }
        });

        btAddCom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textComment = tvCommentaire.getText().toString().trim();
                if (textComment.isEmpty()){
                    DialogFragment fragment = new DialogCommentaire(id_propriete);
                    fragment.show(requireActivity().getSupportFragmentManager(),COMMENT);
                } else {
                    Commentaire comment = new Commentaire(id_propriete, textComment);
                    DialogFragment fragment = new DialogCommentaire(comment);
                    fragment.show(requireActivity().getSupportFragmentManager(),COMMENT);
                }

            }
        });

        btFavOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.proprieteDao().updateIsFavoris(id_propriete);
                    }
                });
            }
        });

        btFavOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.proprieteDao().updateIsNotFavoris(id_propriete);
                    }
                });
            }
        });


        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map2);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this::onMapReady);
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(this);

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        //enableMyLocation();

        proprieteViewModel.getPropriete(id_propriete).observe(getViewLifecycleOwner(), new Observer<Propriete>() {
            @Override
            public void onChanged(Propriete propriete) {
                LatLng position = new LatLng(propriete.getLatitude(), propriete.getLongitude());
                mMap.addMarker(new MarkerOptions().position(position));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 12));
            }
        });

    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED ){
            if (mMap != null){
                mMap.setMyLocationEnabled(true);
            }
        } else {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_PERMISSSION_CODE);
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }
}