package com.h2021.tp3_proprietes.ui.listes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.h2021.tp3_proprietes.R;
import com.h2021.tp3_proprietes.model.Propriete;
import com.h2021.tp3_proprietes.ui.ProprieteViewModel;

import java.util.ArrayList;
import java.util.List;

public class ListesFragment extends Fragment {

    private ProprieteViewModel proprieteViewModel;
    private RecyclerView rvListes;
    private ArrayList<Propriete> lstProprietes;
    private ListesAdapter listesAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        proprieteViewModel =
                new ViewModelProvider(requireActivity()).get(ProprieteViewModel.class);
        View root = inflater.inflate(R.layout.fragment_listes, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvListes = view.findViewById(R.id.rv_listes);
        lstProprietes = new ArrayList<>();
        listesAdapter = new ListesAdapter(lstProprietes);
        rvListes.setLayoutManager(new LinearLayoutManager(requireActivity()));
        rvListes.setAdapter(listesAdapter);

        proprieteViewModel.getLstPropriete().observe(getViewLifecycleOwner(), new Observer<List<Propriete>>() {
            @Override
            public void onChanged(List<Propriete> proprietes) {
                lstProprietes.addAll(proprietes);
                listesAdapter.notifyDataSetChanged();
            }
        });

    }
}