package com.h2021.tp3_proprietes.ui.carte;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.h2021.tp3_proprietes.R;
import com.h2021.tp3_proprietes.model.Propriete;
import com.h2021.tp3_proprietes.ui.ProprieteViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.h2021.tp3_proprietes.R.string.impossible_local;
import static com.h2021.tp3_proprietes.R.string.local_autorise;

public class MapsFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener, View.OnClickListener, GoogleMap.InfoWindowAdapter, GoogleMap.OnMyLocationClickListener, GoogleMap.OnMyLocationButtonClickListener {

    private static final int LOCATION_PERMISSSION_CODE = 1;
    private GoogleMap mMap;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    private Location userLocation;
    private ArrayList<Propriete> lstPropriete;
    private ProprieteViewModel proprieteViewModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        proprieteViewModel = new ViewModelProvider(requireActivity()).get(ProprieteViewModel.class);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    userLocation = location;
                }

            }
        };

        return inflater.inflate(R.layout.fragment_carte, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lstPropriete = new ArrayList<>();

        proprieteViewModel.getLstPropriete().observe(getViewLifecycleOwner(), new Observer<List<Propriete>>() {
            @Override
            public void onChanged(List<Propriete> proprietes) {
                lstPropriete.addAll(proprietes);
            }

        });

        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this::onMapReady);
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(this);

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        enableMyLocation();

        for (Propriete propriete: lstPropriete){
            LatLng position = new LatLng(propriete.getLatitude(),propriete.getLongitude());
            mMap.addMarker(new MarkerOptions().position(position).title(propriete.getType()))
                    .setTag(propriete);
        }

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_PERMISSSION_CODE);
            return;
        }

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            userLocation = location;
                            LatLng user = new LatLng(location.getLatitude(), location.getLongitude());
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(user, 13));
                        }
                    }
                });

    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED ){
            if (mMap != null){
                mMap.setMyLocationEnabled(true);
            }
        } else {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_PERMISSSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(requireActivity(), local_autorise, Toast.LENGTH_LONG).show();
                enableMyLocation();
            } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),Manifest.permission.ACCESS_FINE_LOCATION)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle(R.string.permisson_requise);
                    builder.setMessage(R.string.permission_necessaire);
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_PERMISSSION_CODE);
                        }
                    });
                    builder.setNegativeButton(R.string.annuler, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getContext(), impossible_local,Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.show();
                }
            }
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        Propriete propriete = (Propriete) marker.getTag();

        try {
            NavDirections actionCarte = MapsFragmentDirections.actionNavCarteToDetailsFragment(propriete.getId());
            Navigation.findNavController(getView()).navigate(actionCarte);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

    }


    @Override
    public View getInfoWindow(Marker marker) {
        Location location = new Location(getString(R.string.distance_provider));
        location.setLatitude(marker.getPosition().latitude);
        location.setLongitude(marker.getPosition().longitude);
        if (userLocation != null){
            float distance = userLocation.distanceTo(location);
            Toast.makeText(getContext(),distance/1000 + " km", Toast.LENGTH_LONG).show();
        }
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.marker_carte_layout,null);
        Propriete propriete = (Propriete) marker.getTag();
        TextView tvType, tvPrix, tvAdresse, tvCity;
        ImageView ivPhoto = view.findViewById(R.id.iv_marker_photo);
        tvType = view.findViewById(R.id.tv_marker_type);
        tvPrix = view.findViewById(R.id.tv_marker_prix);
        tvAdresse = view.findViewById(R.id.tv_marker_adresse);
        tvCity = view.findViewById(R.id.tv_maker_city);

        tvType.setText(marker.getTitle());
        tvPrix.setText(propriete.getPrice());
        tvAdresse.setText(propriete.getAdresse());
        tvCity.setText(propriete.getCity());
        Picasso.get().load(propriete.getPhotos().getMoyen())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(ivPhoto);
        return view;
    }


    @Override
    public void onMyLocationClick(@NonNull Location location) {
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onClick(View v) {

    }
}