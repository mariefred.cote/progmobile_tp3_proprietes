package com.h2021.tp3_proprietes.data;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.h2021.tp3_proprietes.model.Commentaire;
import com.h2021.tp3_proprietes.model.Propriete;

@Database(entities = {Propriete.class, Commentaire.class}, version = 1)
public abstract class ProprieteRoomDatabase extends RoomDatabase {

    public static ProprieteRoomDatabase INSTANCE;

    public abstract ProprieteDao proprieteDao();

    public static synchronized ProprieteRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                ProprieteRoomDatabase.class, "propiete_database").build();
        }
        return  INSTANCE;
    }

}
