package com.h2021.tp3_proprietes.data;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.h2021.tp3_proprietes.model.Photos;
import com.h2021.tp3_proprietes.model.Propriete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static java.lang.Integer.parseInt;

public class VolleyUtils {

    private static final String TAG = "TAG";
    private ArrayList<Propriete> proprietes = new ArrayList<>();

    public interface ListProprietesAsyncResponse {
        void processFinished(ArrayList<Propriete> proprieteArrayList);
    }


    public void getProprietes(Context context, ListProprietesAsyncResponse callBack){
        String url ="https://onoup.site/featured-homes.json";

// Request a string response from the provided URL.
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            Propriete propriete = new Propriete();
                            try {
                                JSONObject jsonObject = response.getJSONObject(i);
                                propriete.setAdresse(jsonObject.getString("street"));
                                propriete.setCity(jsonObject.getString("city"));
                                propriete.setId(parseInt(jsonObject.getString("id")));
                                propriete.setLatitude(Double.parseDouble(jsonObject.getString("latitude")));
                                propriete.setLongitude(Double.parseDouble(jsonObject.getString("longitude")));
                                JSONObject jsonPhoto = jsonObject.getJSONObject("photos");
                                Photos photos = new Photos(
                                        jsonPhoto.getString("alternative_text"),
                                        jsonPhoto.getString("_155"),
                                        jsonPhoto.getString("_600"),
                                        jsonPhoto.getString("_1600")
                                );
                                propriete.setPhotos(photos);
                                propriete.setPrice(jsonObject.getString("price_display"));
                                propriete.setType(jsonObject.getString("type"));
                                propriete.setFavoris(false);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            proprietes.add(propriete);
                        }
                        if (callBack != null) callBack.processFinished(proprietes);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+ error);
            }
        });


// Add the request to the RequestQueue.
        MySingleton.getInstance(context).addToRequestQueue(jsonArrayRequest);


    }
}
