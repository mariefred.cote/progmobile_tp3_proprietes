package com.h2021.tp3_proprietes.data;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.h2021.tp3_proprietes.model.Commentaire;
import com.h2021.tp3_proprietes.model.Propriete;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface ProprieteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Propriete propriete);

    @Query("SELECT * FROM propriete_table")
    List<Propriete> getProprietes();

    @Query("SELECT * FROM propriete_table")
    LiveData<List<Propriete>> getProprietesLive();

    @Query("SELECT * FROM propriete_table WHERE favoris")
    LiveData<List<Propriete>> getProprietesFav();

    @Query("SELECT * FROM propriete_table WHERE id = :id")
    LiveData<Propriete> getPropriete(int id);

    @Query("UPDATE propriete_table SET favoris = 1 WHERE id = :id")
    void updateIsFavoris(int id);

    @Query("UPDATE propriete_table SET favoris = 0 WHERE id = :id")
    void updateIsNotFavoris(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Commentaire commentaire);

    @Query("SELECT * FROM commentaires_table WHERE idPropriete = :id")
    LiveData<Commentaire> getCommentaire(int id);

    @Query("DELETE FROM commentaires_table")
    void deleteAllCommentaire();

}
