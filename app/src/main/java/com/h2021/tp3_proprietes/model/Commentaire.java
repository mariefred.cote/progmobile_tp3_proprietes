package com.h2021.tp3_proprietes.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "commentaires_table")
public class Commentaire {

    @PrimaryKey
    private int idPropriete;

    @NonNull
    private String commentaire;

    public Commentaire(int idPropriete, @NonNull String commentaire) {
        this.idPropriete = idPropriete;
        this.commentaire = commentaire;
    }

    public int getIdPropriete() {
        return idPropriete;
    }

    public void setIdPropriete(int idPropriete) {
        this.idPropriete = idPropriete;
    }

    @NonNull
    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(@NonNull String commentaire) {
        this.commentaire = commentaire;
    }
}
