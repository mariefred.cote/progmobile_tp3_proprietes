package com.h2021.tp3_proprietes.model;

public class Photos {

    private String alternative_text;
    private String petit;
    private String moyen;
    private String grand;

    public Photos(String alternative_text, String petit, String moyen, String grand) {
        this.alternative_text = alternative_text;
        this.petit = petit;
        this.moyen = moyen;
        this.grand = grand;
    }

    public String getAlternative_text() {
        return alternative_text;
    }

    public void setAlternative_text(String alternative_text) {
        this.alternative_text = alternative_text;
    }

    public String getPetit() {
        return petit;
    }

    public void setPetit(String petit) {
        this.petit = petit;
    }

    public String getMoyen() {
        return moyen;
    }

    public void setMoyen(String moyen) {
        this.moyen = moyen;
    }

    public String getGrand() {
        return grand;
    }

    public void setGrand(String grand) {
        this.grand = grand;
    }
}
